/*
 * package test;
 * 
 * import java.io.BufferedReader; import java.io.IOException; import
 * java.io.InputStreamReader; import java.io.PrintWriter; import
 * java.time.Duration; import java.time.LocalDate; import
 * java.time.LocalDateTime; import java.time.LocalTime;
 * 
 * public class Test {
 * 
 * public static void main(String[] args) throws IOException {
 * 
 * BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
 * PrintWriter wr = new PrintWriter(System.out); String r = br.readLine(); int n
 * = Integer.parseInt(br.readLine().trim()); String[] p = new String[n]; for
 * (int i_p = 0; i_p < n; i_p++) { p[i_p] = br.readLine(); }
 * 
 * String[] out_ = solve(p, r); for (int i_out_ = 0; i_out_ < out_.length;
 * i_out_++) {
 * 
 * LocalTime arrayTime = LocalTime.parse(out_[i_out_]);
 * 
 * if (arrayTime.getHour() == 0 && arrayTime.getMinute() == 0 &&
 * arrayTime.getSecond() == 0) { System.out.println("now"); }else
 * if(arrayTime.getSecond() != 0 && arrayTime.getSecond() < 60) {
 * System.out.println(arrayTime.getSecond() + " seconds ago."); }else
 * if(arrayTime.getMinute() != 0 && arrayTime.getMinute() < 60 ) {
 * System.out.println(arrayTime.getMinute() + " minutes ago."); }
 * 
 * 
 * }
 * 
 * wr.close(); br.close();
 * 
 * }
 * 
 * static String[] solve(String[] p, String r) {
 * 
 * String[] outputTimeArray = new String[p.length];
 * 
 * for (int i = 0; i < p.length; i++) {
 * 
 * LocalTime t1 = LocalTime.parse(p[i]); LocalTime t2 = LocalTime.parse(r);
 * 
 * LocalDateTime dateTime1 = LocalDateTime.of(LocalDate.now(), t1);
 * LocalDateTime dateTime2 = LocalDateTime.of(LocalDate.now(), t2);
 * 
 * outputTimeArray[i] = calculateDifference(dateTime1, dateTime2);
 * 
 * }
 * 
 * return outputTimeArray;
 * 
 * }
 * 
 * private static String calculateDifference(LocalDateTime timeStart,
 * LocalDateTime timeStop) { Duration duration = Duration.between(timeStart,
 * timeStop); return durationOutput(duration); }
 * 
 * private static String durationOutput(Duration duration) { long hours =
 * duration.toHours(); long minutes = duration.toMinutes() - (hours * 60); long
 * seconds = duration.getSeconds() - (hours * 3600) - (minutes * 60);
 * 
 * return (timeUnitsOutput(hours) + ":" + timeUnitsOutput(minutes) + ":" +
 * timeUnitsOutput(seconds)); }
 * 
 * private static String timeUnitsOutput(long units) { return (units < 10) ?
 * ("0" + units) : String.valueOf(units); } }
 */