package test;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.autoconfigure.security.servlet.WebSecurityEnablerConfiguration;

public class AppTest extends WebSecurityEnablerConfiguration{

	public static List<Integer> splitInteger(Integer num, Integer parts) {

		List<Integer> finalList = new ArrayList<>();

		if (num < parts) {
			finalList.add(num);

		} else if (num % parts == 0) {
			for (int i = 0; i < parts; i++)
				System.out.print((num / parts) + " ");
		} else {
			int zp = parts - (num % parts);
			int pp = num / parts;
			for (int i = 0; i < parts; i++) {

				if (i >= zp) {
					System.out.print((pp + 1) + " ");
					finalList.add(pp + 1);
				} else {
					System.out.print(pp + " ");
					finalList.add(pp);
				}
			}
		}

		return finalList;
	}
	
	
	
	
}
