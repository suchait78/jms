package com.activemq.async;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class AsyncProcessing {

	@Async
	public void startAsyncProcessing() {
		System.out.println("Async processing started - " + Thread.currentThread().getId());		
	}
}
