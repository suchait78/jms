package com.activemq.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.activemq.jms.cookie.TestProcessor;
import com.activemq.pojo.Student;

@Controller
@Path("/cookie")
public class JController {

	@Autowired
	TestProcessor testProcessor;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/get/data")
	public Response getResponse() {

		//testProcessor.setHttpServletResponseValue(servletResponse);

		Student student = new Student(12L, "suchait");
		ResponseBuilder responseBuilder = Response.status(Response.Status.OK).entity(student);

		// JerseyResponseCookieAdapter adapter = new
		// JerseyResponseCookieAdapter(servletResponse, responseBuilder);

		// Response response = adapter.getJerseyResponse();

		Response response = responseBuilder.build();

		return response;
	}

}
