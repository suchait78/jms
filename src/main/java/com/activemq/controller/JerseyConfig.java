package com.activemq.controller;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;


@ApplicationPath("/demo-rest")
@Configuration
public class JerseyConfig extends ResourceConfig{

	public JerseyConfig() {
		register(JController.class);
	}
}
