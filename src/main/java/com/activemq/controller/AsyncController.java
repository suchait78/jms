/*
 * package com.activemq.controller;
 * 
 * import org.slf4j.Logger; import org.slf4j.LoggerFactory; import
 * org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.web.bind.annotation.PostMapping; import
 * org.springframework.web.bind.annotation.RestController;
 * 
 * import com.activemq.async.AsyncProcessing;
 * 
 * @RestController public class AsyncController {
 * 
 * private final static Logger logger =
 * LoggerFactory.getLogger(AsyncController.class);
 * 
 * 
 * @Autowired AsyncProcessing asyncProcessing;
 * 
 * @PostMapping(path = "/start-async/processing") public void
 * startAsyncProcessing() {
 * logger.info("inside async processing controller method.");
 * 
 * asyncProcessing.startAsyncProcessing(); } }
 */