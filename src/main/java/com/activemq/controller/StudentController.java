package com.activemq.controller;

import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.activemq.async.AsyncProcessing;
import com.activemq.exception.StudentNotFoundException;
import com.activemq.jms.cookie.CookieHelper;
import com.activemq.jms.cookie.JerseyResponseCookieAdapter;
import com.activemq.jms.cookie.TestProcessor;
import com.activemq.jpa.StudentRepository;
import com.activemq.pojo.Student;

@RestController
public class StudentController {

	public static final Logger logger = LoggerFactory.getLogger(StudentController.class);

	@Autowired
	StudentRepository studentService;

	@Autowired
	AsyncProcessing asyncProcessing;

	@Autowired
	TestProcessor testProcessor;
	
	@GetMapping(path = "/hello-world")
	public String myString() {
		return "hello-world";
	}

	@GetMapping(path = "/students-test/{name}")
	public Student getMethodUsingHelper(@PathVariable String name, HttpServletResponse servletResponse) {

		Cookie cookie = new Cookie("test-cookie","test-value");
		cookie.setDomain("test-domain");
		cookie.setHttpOnly(true);
		cookie.setPath("/test/path");
		cookie.setSecure(true);
				
		CookieHelper.addCookie(servletResponse, cookie, "none");
		
		return new Student(12l, name);
	}
		
	@GetMapping(path = "/students/{name}")
	public Response getMethod(@PathVariable String name, HttpServletResponse servletResponse) {

		testProcessor.setHttpServletResponseValue(servletResponse);
		
		Student student = new Student(12L, name);
		ResponseBuilder responseBuilder = Response.status(Response.Status.OK).entity(student);
		
		//JerseyResponseCookieAdapter adapter = new JerseyResponseCookieAdapter(servletResponse, responseBuilder);
		
		//Response response = adapter.getJerseyResponse();
		
		Response response = responseBuilder.build();
		
		return response;
	}

	@GetMapping(path = "get-student/{id}")
	public Student getStudent(@PathVariable Long id) throws StudentNotFoundException {
		Student student = studentService.findById(id);

		if (student == null) {
			throw new StudentNotFoundException("Student Not Found.");
		}

		return student;
	}

	@GetMapping(path = "get-all-student")
	public List<Student> getAllStudents() {
		return studentService.getAllStudents();
	}

	@PostMapping(path = "/add-student")
	public void addStudent(@Valid @RequestBody Student student) {
		studentService.save(student);
	}

	@PostMapping(path = "/start-async/processing")
	public void startAsyncProcessing() {
		// logger.info("inside async processing controller method.");

		asyncProcessing.startAsyncProcessing();
	}
}
