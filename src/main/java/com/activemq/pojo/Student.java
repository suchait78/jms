package com.activemq.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@NotNull
@Entity
public class Student {

	@Id
	@GeneratedValue
	private Long id;
	
	@NotNull
	@Size(min=2, message = "student name cannot be less than 2 words.")
	@Column(nullable = false)
	private String studentName;
	
	public Student() {
		
	}
	
	public Student(Long id, String studentName) {
		super();
		this.id = id;
		this.studentName = studentName;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
		
}
