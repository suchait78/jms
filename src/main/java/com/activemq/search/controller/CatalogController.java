package com.activemq.search.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.activemq.search.jpa.ProductRepository;
import com.activemq.search.pojo.Brand;
import com.activemq.search.pojo.Product;

@RestController
public class CatalogController {

	@Autowired
	ProductRepository p;

	public Product getProductByName() {		
		return null;
	}
	
	@GetMapping(path = "/brand/{id}")
	public Brand getBrand(@PathVariable Integer id) {
		return p.searchByBrandId(id);
	}
	
	@GetMapping(path = "/product/{id}")
	public Product getProductById(@PathVariable Integer id) {
		
		Product product = p.ProductById(id);
		return product;
	}
	
	@GetMapping(path = "/allproducts/brand/{id}")
	public List<Product> getBrandAllProducts(@PathVariable Integer id) {
		
		Brand brand = p.searchByBrandId(id);		
		return brand.getProduct();	
	}
	
}
