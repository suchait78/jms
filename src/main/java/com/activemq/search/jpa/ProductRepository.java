package com.activemq.search.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.activemq.search.pojo.Brand;
import com.activemq.search.pojo.Color;
import com.activemq.search.pojo.Product;
import com.activemq.search.pojo.Seller;
import com.activemq.search.pojo.Size;

@Repository
@Transactional
public class ProductRepository {

	@PersistenceContext
	EntityManager em;

	public void createProduct(Product product) {

		em.persist(product);
	}

	public Brand searchByBrandId(Integer id) {
		return em.find(Brand.class, id);
	}

	public Product ProductById(Integer id) {
		return em.find(Product.class, id);
	}

	public List<Product> ProductByName(String name) {

		TypedQuery<Product> query = em.createQuery("select p from Product p where productName := pname", Product.class);
		query.setParameter("pname", name);

		return query.getResultList();
	}

	public void createSeller(Seller seller) {
		em.persist(seller);
	}

	public void createSize(Size size) {
		em.persist(size);
	}

	public void createColor(Color color) {
		em.persist(color);
	}

	public void createBrand(Brand brand) {
		em.persist(brand);
	}

}
