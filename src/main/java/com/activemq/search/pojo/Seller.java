package com.activemq.search.pojo;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Seller {

	@Id
	@GeneratedValue
	private Integer sellerId;
	private String sellerName;
	private String sellerAddress;
	
	@ManyToMany(mappedBy = "seller", cascade = CascadeType.ALL)
	private List<Product> product;
	
	public Seller() {
		// TODO Auto-generated constructor stub
	}


	public Seller(String sellerName, String sellerAddress) {
		super();
		this.sellerName = sellerName;
		this.sellerAddress = sellerAddress;
	}


	public Integer getSellerId() {
		return sellerId;
	}


	public void setSellerId(Integer sellerId) {
		this.sellerId = sellerId;
	}


	public String getSellerName() {
		return sellerName;
	}


	public void setSellerName(String sellerName) {
		this.sellerName = sellerName;
	}


	public String getSellerAddress() {
		return sellerAddress;
	}


	public void setSellerAddress(String sellerAddress) {
		this.sellerAddress = sellerAddress;
	}

}
