package com.activemq.search.pojo;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Size {

	@Id
	@GeneratedValue
	private Integer sizeId;
	private String sizeValue;
	
	@ManyToMany(mappedBy = "size", cascade = CascadeType.ALL)
	private List<Product> product;
	
	public Size() {
		// TODO Auto-generated constructor stub
	}

	public Size(String sizeValue) {
		super();
		this.sizeValue = sizeValue;
	}

	public Integer getSizeId() {
		return sizeId;
	}

	public void setSizeId(Integer sizeId) {
		this.sizeId = sizeId;
	}

	public String getSizeValue() {
		return sizeValue;
	}

	public void setSizeValue(String sizeValue) {
		this.sizeValue = sizeValue;
	}
	
}
