package com.activemq.search.pojo;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Product {

	@Id
	@GeneratedValue
	private Integer productId;
	
	private String productName;
	
	@ManyToMany
	private List<Seller> seller = new ArrayList<>();
	
	@ManyToMany
	private List<Color> color = new ArrayList<>();
	
	@JsonIgnore
	@OneToOne
	private Brand brand;
	
	@ManyToMany
	private List<Size> size = new ArrayList<>();

	@CreationTimestamp
	private LocalDateTime createTime;
	
	public Product() {
		// TODO Auto-generated constructor stub
	}
	
	public Product(String productName) {
		super();
		this.productName = productName;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public List<Seller> getSeller() {
		return seller;
	}

	public void setSeller(List<Seller> seller) {
		this.seller = seller;
	}

	public List<Color> getColor() {
		return color;
	}

	public void setColor(List<Color> color) {
		this.color = color;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}

	public List<Size> getSize() {
		return size;
	}

	public void setSize(List<Size> size) {
		this.size = size;
	}	
}

