package com.activemq.search.pojo;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Color {

	@Id
	@GeneratedValue
	private Integer colorId;
	private String colorName;
	
	@ManyToMany(mappedBy = "color", cascade = CascadeType.ALL)
	private List<Product> product;
	
	public Color() {
		// TODO Auto-generated constructor stub
	}

	public Color(String colorName) {
		super();
		this.colorName = colorName;
	}

	public Integer getColorId() {
		return colorId;
	}

	public void setColorId(Integer colorId) {
		this.colorId = colorId;
	}

	public String getColorName() {
		return colorName;
	}

	public void setColorName(String colorName) {
		this.colorName = colorName;
	}
	
}
