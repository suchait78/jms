package com.activemq.exception;

import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class ApplicationExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {

		ApplicationException applicationException = new ApplicationException(new Date(), ex.getMessage(),
				request.getDescription(false));

		return new ResponseEntity<Object>(applicationException, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(StudentNotFoundException.class)
	public final ResponseEntity<Object> handleStudentNotFoundException(Exception ex, WebRequest request) {

		ApplicationException applicationException = new ApplicationException(new Date(), ex.getMessage(),
				request.getDescription(false));

		return new ResponseEntity<Object>(applicationException, HttpStatus.NOT_FOUND);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		ApplicationException applicationException = new ApplicationException(new Date(),
				"Validated failed on student object",
				ex.getBindingResult().toString());

		return new ResponseEntity<Object>(applicationException, HttpStatus.BAD_REQUEST);
	}

}
