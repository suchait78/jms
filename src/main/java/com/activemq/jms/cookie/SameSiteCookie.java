package com.activemq.jms.cookie;

import javax.ws.rs.core.NewCookie;

public class SameSiteCookie extends NewCookie {

	private final String sameSite;

	public SameSiteCookie(String name, String value, String sameSite) {
		super(name,value);
		this.sameSite = sameSite;
	}
	
	public SameSiteCookie(String name, String value, String path, String domain, String comment, int maxAge,
			boolean secure, boolean httpOnly, String sameSite) {
		super(name, value, path, domain, comment, maxAge, secure, httpOnly);
		this.sameSite = sameSite;
	}

	public String getSameSite() {
		return sameSite;
	}

	@Override
	public String toString() {
		String retVal = super.toString();

		if (retVal.length() > 0) {

			if (this.sameSite != null) {

				if (this.sameSite.length() > 0) {
					retVal = String.format("%s;SameSite=%s", retVal, this.sameSite);
				}
			}
		}
		return retVal;

	}
}
