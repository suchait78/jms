package com.activemq.jms.cookie;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

public class JerseyResponseCookieAdapter {

	private static final String SET_COOKIE = "Set-Cookie";
	private static final String SAMESITE = "none";
	private static final String EQUALS_SPLITTER = "=";
	private static final String SEMICOLON_SPLITTER = "=";

	private HttpServletResponse servletResponse;
	private ResponseBuilder jerseyResponseBuilder;

	public JerseyResponseCookieAdapter(HttpServletResponse servletResponse, ResponseBuilder responseBuilder) {
		this.servletResponse = servletResponse;
		this.jerseyResponseBuilder = responseBuilder;
	}

	private Map<String, String> prepareCookiesMap() {

		Map<String, String> cookieMap = new LinkedHashMap<>();

		String[] cookiesInHttpServletResponse = Arrays
				.stream(servletResponse.getHeader(SET_COOKIE).split(SEMICOLON_SPLITTER)).map(String::trim)
				.toArray(String[]::new);

		Arrays.asList(cookiesInHttpServletResponse).stream().forEach(cookieEntry -> {

			if (cookieEntry.contains(EQUALS_SPLITTER)) {
				String[] cookieKeyAndValue = Arrays.stream(cookieEntry.split(EQUALS_SPLITTER)).map(String::trim)
						.toArray(String[]::new);

				cookieMap.put(cookieKeyAndValue[0], cookieKeyAndValue[1]);

			} else {
				cookieMap.put(cookieEntry, "true");
			}

		});

		servletResponse.reset();

		return cookieMap;
	}

	public Response getJerseyResponse() {

		Map<String, String> cookieMap = prepareCookiesMap();
		
		Map.Entry<String, String> firstEntryInMap = cookieMap.entrySet().stream().findFirst().get();

		SameSiteCookie sameSiteCookie = new SameSiteCookie(firstEntryInMap.getKey(), firstEntryInMap.getValue(),
				SAMESITE);

		jerseyResponseBuilder.cookie(sameSiteCookie);

		return jerseyResponseBuilder.build();
	}
}
