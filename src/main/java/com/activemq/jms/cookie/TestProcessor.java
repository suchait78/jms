package com.activemq.jms.cookie;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

@Component
public class TestProcessor {
	
	public void setHttpServletResponseValue(HttpServletResponse servletResponse) {
		Cookie c = new Cookie("test-cookie","test-value");
		c.setPath("/test/path");
		c.setSecure(true);
		c.setHttpOnly(true);
		c.setDomain("Domain-1");
		
		servletResponse.addCookie(c);
	}
}
