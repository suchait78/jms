package com.activemq.jms;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

import com.activemq.search.jpa.ProductRepository;
import com.activemq.search.pojo.Brand;
import com.activemq.search.pojo.Color;
import com.activemq.search.pojo.Product;
import com.activemq.search.pojo.Seller;
import com.activemq.search.pojo.Size;

@ComponentScan("com.activemq")
@EntityScan({ "com.activemq.search.pojo" })
@SpringBootApplication
@EnableDiscoveryClient
public class JmsApplication implements CommandLineRunner {

	@Autowired
	ProductRepository productRepo;

	public static void main(String[] args) {
		SpringApplication.run(JmsApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		Seller seller = new Seller("test-seller", "test-seller-address");
		productRepo.createSeller(seller);

		List<Seller> sellerList = Arrays.asList(seller);

		Brand brand = new Brand("test-brand");
		productRepo.createBrand(brand);

		Color color = new Color("blue");
		productRepo.createColor(color);

		List<Color> colorList = Arrays.asList(color);

		Size size = new Size("M");
		productRepo.createSize(size);

		List<Size> sizeList = Arrays.asList(size);

		Product product = new Product("Shirt");
		product.setBrand(brand);
		product.setColor(colorList);
		product.setSeller(sellerList);
		product.setSize(sizeList);

		productRepo.createProduct(product);

	}

}
