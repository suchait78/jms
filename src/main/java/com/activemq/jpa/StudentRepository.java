package com.activemq.jpa;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.activemq.pojo.Student;

@Repository
@Transactional
public class StudentRepository {

	@PersistenceContext
	EntityManager em;

	public Student findById(Long id) {

		Optional<Student> student = Optional.ofNullable(em.find(Student.class, id));
		if (student.isPresent()) {
			return student.get();
		}
		return null;
	}

	public Student save(Student student) {
		
		Student studentMerged = em.merge(student);
		return studentMerged;
	}
	
	public List<Student> getAllStudents(){
		TypedQuery<Student> query = em.createQuery("select s from Student s", Student.class);
		return query.getResultList();
	}
	
}
